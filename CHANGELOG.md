# v0.9.5

* Tweaked difficulty by making upcoming objects smaller overall.
* Tweaked transparency on foreground objects for better contrast.

# v0.9.4

* Added the ability to toggle the control scheme with F3.
* Added window title.


# v0.9.3

* Code cleanup and minor fixes.


# v0.9.2

* Added a music toggle, switch music on or off with F2.
* Tweaked palette generation for more colorful results.


# v0.9.1

* Implemented resolution switching with F4.
