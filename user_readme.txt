=== Intro ===

Hi! Thanks for trying out my dumb little game. At first I started working on it as a way
to familiarize myself with Allegro (a multimedia library for C/C++) and get some much
needed practice with software development in general.

I wasn't really planning on realeasing this. You can clearly tell this is something made
by a beginner both for fun and for learning purposes (collision is rudimentary; there was
no need to implement random palettes, but I did it anyway because I could). Eventually I
came up with some neat ideas and thought I may as well get this out there, since it was
starting to resemble an actual game. I hope you enjoy at least for a little while!


=== Controls ===

← and → or A and D -- Move horizontally.

↓ and ↑ or S and W-- Shoot down and up.

Left Shift or Right Shift -- Slow down movement speed.

Enter -- Pause/unpause.

F2 -- Music toggle.

F3 -- Control scheme toggle.

F4 -- Resolution swapping. (640x480 ←→ 800x600)


=== How to play ===

Dodge or shoot the upcoming objects. If you get hit, you lose.

The bar at the bottom represents your energy and depletes over time, keep shooting to
fill it back up. If it gets empty, you lose.

The game gets increasingly difficult as you go on, don't get too comfy.


=== Q&A's ===

Q: "Why are the controls so annoying?!"
A: The layout is on purpose. Schütt is focused on tight coordination between dodging and
   precise shooting. You'll get used to it.

Q: "Why is the resolution so small?"
A: Press F4. That'll scale the resolution up to 800x600. Press it again to go back down to
   640x480.

Q: "That didn't answer my question!"
A: Any bigger and I wouldn't have been able to develop and test the game. I only have a
   small, 32-bit Atom laptop w/ 2 GiB of RAM as my machine for... well, everything.

Q: "Can I use your assets?"
A: Sure, under this license: https://creativecommons.org/licenses/by-nc-sa/4.0/

Q: "Can I borrow your source?"
A: No need to ask me. Code is licensed under the GNU GPLv3. Check the details in the
   included COPYING file.

Q: "Gamepad support?"
A: No.

Q: "Is that a vinesauce reference?"
A: Maybe.


=== Tips ===

* The game's title is a lie! You need to know when to SCHÜTT, and when to focus on
  dodging.

* Size matters.
