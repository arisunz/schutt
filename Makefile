PKGDATA = assets/fonts/FiraMono-Bold.otf \
		  assets/fonts/FiraMono-Medium.otf \
		  assets/sounds/pew.ogg \
		  assets/sounds/expl.ogg \
		  assets/sounds/toggle.ogg \
		  assets/sounds/bgm.ogg \
		  assets/gfx/icon.png \
		  COPYING.md README.md CHANGELOG.md user_readme.txt

DEPS = src/*.h

SRCFILES = $(DEPS) \
		   src/*.c

OBJS = src/draw.o \
	   src/ebar.o \
	   src/engine.o \
	   src/floatrect.o \
	   src/input.o \
	   src/main.o \
	   src/meteor.o \
	   src/palette.o \
	   src/particle.o \
	   src/player.o \
	   src/projectile.o \
	   src/update.o \

CFLAGS = -Wall -O0 -g
LDFLAGS = -lallegro \
		 -lallegro_primitives \
		 -lallegro_font \
		 -lallegro_ttf \
		 -lallegro_color \
		 -lallegro_audio \
		 -lallegro_acodec \
		 -lallegro_image

BASE_FLAGS = $(CFLAGS) $(LDFLAGS)

CC = gcc
MINGW32CC = i686-w64-mingw32-gcc
MINGW64CC = x86_64-w64-mingw32-gcc

VERSION = `git describe --tags`

debug: $(OBJS)
	$(CC) $(OBJS) -o schutt $(BASE_FLAGS)

src/%.o: src/%.c $(DEPS)
	$(CC) -c $< -o $@ $(BASE_FLAGS)

release:
	$(CC) src/*.c $(LDFLAGS) -Wall -O3 -o schutt

package: release
	tar -cvzf schutt_linux_$(VERSION).tar.gz \
		$(PKGDATA) \
		$(SRCFILES) \
		schutt

buildw32:
	$(MINGW32CC) src/*.c -Wall -O3 \
    -I./mingw/i686/include -L./mingw/i686/lib \
    -lallegro_monolith.dll \
    -o schutt_w32.exe

buildw64:
	$(MINGW64CC) src/*.c -Wall -O3 \
    -I./mingw/x86_64/include -L./mingw/x86_64/lib \
    -lallegro_monolith.dll \
    -o schutt_w64.exe

packagew32: buildw32
	cp mingw/i686/bin/allegro_monolith-5.2.dll .
	cp mingw/i686/bin/libwinpthread-1.dll .
	cp mingw/i686/bin/libstdc++-6.dll .
	cp mingw/i686/bin/libgcc_s_dw2-1.dll .
	-rm -f schutt_w32*.zip
	zip schutt_w32_$(VERSION).zip \
		$(PKGDATA) \
		$(SRCFILES) \
		allegro_monolith-5.2.dll libwinpthread-1.dll libstdc++-6.dll libgcc_s_dw2-1.dll \
		schutt_w32.exe

packagew64: buildw64
	cp mingw/x86_64/bin/allegro_monolith-5.2.dll .
	cp mingw/x86_64/bin/libwinpthread-1.dll .
	cp mingw/x86_64/bin/libstdc++-6.dll .
	cp mingw/x86_64/bin/libgcc_s_seh-1 .
	-rm -f schutt_w64*.zip
	zip schutt_w64_$(VERSION).zip \
		$(PKGDATA) \
		$(SRCFILES) \
		allegro_monolith-5.2.dll libwinpthread-1.dll libstdc++-6.dll libgcc_s_seh-1 \
		schutt_w64.exe

clean:
	-rm -f $(OBJS) *.zip *.gz core schutt *.exe *.dll

rebuild: clean all
