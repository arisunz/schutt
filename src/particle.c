/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <time.h>
#include "particle.h"

void expl_create(PSystem* psystem, Vector2f pos, ALLEGRO_COLOR color) {
    srand(time(NULL));
    for (uint32_t i = 0; i < MAX_PARTICLES; ++i) {
        psystem->direction[i].x = ((rand() % 601) + 1) * (rand() % 2 == 0 ? (1.0 / 60) : -(1.0 / 60));
        psystem->direction[i].y = ((rand() % 601) + 1) * (rand() % 2 == 0 ? (1.0 / 60) : -(1.0 / 60));
    }

    for (uint32_t i = 0; i < MAX_PARTICLES * 3; i += 3) {
        uint32_t size = rand() % 21 + 6;

        psystem->shapes[i].x = rand() % 2 == 0 ? pos.x - (rand() % size) : pos.x + (rand() % size);
        psystem->shapes[i].y = pos.y - (rand() % size);
        psystem->shapes[i].z = 0;
        psystem->shapes[i].color = color;

        psystem->shapes[i + 1].x = pos.x + (rand() % size);
        psystem->shapes[i + 1].y = pos.y + (rand() % size);
        psystem->shapes[i + 1].z = 0;
        psystem->shapes[i + 1].color = color;

        psystem->shapes[i + 2].x = pos.x - (rand() % size);
        psystem->shapes[i + 2].y = pos.y + (rand() % size);
        psystem->shapes[i + 2].z = 0;
        psystem->shapes[i + 2].color = color;
    }

    psystem->lifetime = 0;
    psystem->active = true;
}

void expl_update(PSystem* psystem) {
    for (uint32_t i = 0; i < MAX_PARTICLES * 3; i += 3) {
        uint32_t offset_index = i ? (i+1) / 3 - 1 : i;
        float xoffset = psystem->direction[offset_index].x;
        float yoffset = psystem->direction[offset_index].y;

        psystem->shapes[i].x += xoffset;
        psystem->shapes[i].y += yoffset;
        psystem->shapes[i + 1].x += xoffset;
        psystem->shapes[i + 1].y += yoffset;
        psystem->shapes[i + 2].x += xoffset;
        psystem->shapes[i + 2].y += yoffset;
    }

    ++psystem->lifetime;
    if (psystem->lifetime > PARTICLE_MAX_LIFETIME) {
        psystem->active = false;
    }
}

bool lifetime_over(PSystem* psystem) {
    return psystem->lifetime > PARTICLE_MAX_LIFETIME;
}
