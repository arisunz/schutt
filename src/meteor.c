/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include "globdefs.h"
#include "meteor.h"

void meteor_shoot(Meteor* mt, uint32_t speed, bool shooting_up, uint32_t seed) {
    srand(seed);
    mt->position.x = rand() % (SCREEN_W + 1);
    mt->position.y = shooting_up ? SCREEN_H + 30 : -30;

    mt->radius = rand() % METEOR_SIZE_MAX;
    if (mt->radius < METEOR_SIZE_MIN) {
        mt->radius = METEOR_SIZE_MIN;
    }

    mt->hitbox.top = mt->position.y - mt->radius;
    mt->hitbox.left = mt->position.x - mt->radius;
    mt->hitbox.height = mt->radius * 2;
    mt->hitbox.width = mt->radius * 2;

    mt->speed = speed;

    mt->shooting_up =  shooting_up;
    mt->active = true;
}

void meteor_move(Meteor* mt, float yoffset) {
    if (mt->shooting_up) {
        mt->position.y += -yoffset;
        mt->hitbox.top += -yoffset;
    } else {
        mt->position.y += yoffset;
        mt->hitbox.top += yoffset;
    }

    if (mt->position.y < -50 || mt->position.y > SCREEN_H + 50) {
        mt->active = false;
    }
}
