/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "engine.h"

void random_meteor_shoot(Meteor mt[static MAX_METEORS], uint32_t speed, uint32_t seed) {
    srand(time(NULL));
    for (uint32_t i = 0; i < MAX_METEORS; ++i) {
        if (!mt[i].active) {
            meteor_shoot(&mt[i], speed, rand() % 2 == 0 ? true : false, seed);
            break;
        }
    }
}

void gc_update(GameComponents* gc) {
    float proj_yoffset = PROJECTILE_ACCEL * (1.0 / MAX_FPS);
    for (uint32_t i = 0; i < MAX_PROJECTILES; ++i) {
        if (gc->projectiles[i].active) {
            projectile_move(&gc->projectiles[i], proj_yoffset);
        }
    }

    for (uint32_t i = 0; i < MAX_METEORS; ++i) {
        if (gc->meteors[i].active) {
            meteor_move(&gc->meteors[i], gc->meteors[i].speed * (1.0 / MAX_FPS));
        }

        if (!gc->player.expired && rect_intersect(&gc->player.hitbox, &gc->meteors[i].hitbox)) {
            gc->player.expired = true;
            for (uint32_t i = 0; i < MAX_PARTICLE_CLUSTER; ++i) {
                if (!gc->pscluster[i].active) {
                    expl_create(&gc->pscluster[i], gc->player.position, gc->palette.fg);
                    gc->player.expired_explosion = &gc->pscluster[i];
                }
            }
            al_play_sample(gc->sounds.expl, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
        }
    }

    for (uint32_t i = 0; i < MAX_PARTICLE_CLUSTER; ++i) {
        if (gc->pscluster[i].active) {
            expl_update(&gc->pscluster[i]);
        }
    }

    if (ebar_update(&gc->player.ebar) && !gc->player.expired) {
        gc->player.expired = true;
        for (uint32_t i = 0; i < MAX_PARTICLE_CLUSTER; ++i) {
            if (!gc->pscluster[i].active) {
                expl_create(&gc->pscluster[i], gc->player.position, gc->palette.fg);
                gc->player.expired_explosion = &gc->pscluster[i];
                al_play_sample(gc->sounds.expl, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
            }
        }
    }


    if (gc->player.expired && gc->player.expired_explosion->lifetime > PARTICLE_MAX_LIFETIME) {
        gc->state = GAME_OVER;
        if (al_get_sample_instance_playing(gc->sounds.bgm_instance)) {
            al_set_sample_instance_gain(gc->sounds.bgm_instance, 0.4);
        }
    }

}


void collision_check(GameComponents* gc) {
    /*
     * enter the
     * B U L L S H I T   L O O P
     *
     * Actually, it only checks bullet-meteor collision.
     * Meteor-player collision is checked in gc_update()
     * because this was big enough as it is.
     */
    for (uint32_t i = 0; i < MAX_PROJECTILES; ++i) {
        if (gc->projectiles[i].active) {

            for (uint32_t j = 0; j < MAX_METEORS; ++j) {
                if (gc->meteors[j].active) {

                    if (point_intersect(&gc->meteors[j].hitbox, &gc->projectiles[i].proj_start)) {
                        for (uint32_t k = 0; k < MAX_PARTICLE_CLUSTER; ++k) {

                            if (!gc->pscluster[k].active && gc->pscluster[k].lifetime == 0) {
                                expl_create(&gc->pscluster[k], gc->meteors[j].position, gc->palette.fg_transp);
                                break;
                            }
                        }

                        gc->score += gc->meteors[j].radius * 100;

                        //ebar_refill(&gc->player.ebar, (SCREEN_W - 20) * 0.1 + gc->meteors[j].radius);
                        ebar_refill(&gc->player.ebar, gc->meteors[j].radius * 3);

                        al_play_sample(gc->sounds.expl, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

                        gc->projectiles[i].active = false;
                        gc->meteors[j].active = false;

                        break; // don't let this now inactive projectile hit more meteors
                    }
                }
            }
        }
    }
}

void game_reset(GameComponents* gc, ALLEGRO_DISPLAY* display) {
    palette_get(&gc->palette);
    al_set_target_bitmap(gc->wall);
    al_clear_to_color(gc->palette.bg);
    al_set_target_backbuffer(display);

    gc->score = 0;
    gc->timers.session_time = 0;
    gc->state = RUNNING;
    gc->mt_base_speed = 200;

    gc->timers.shfreq = 1;

    player_init(&gc->palette, &gc->player);

    memset(&gc->meteors, 0, sizeof(Meteor) * MAX_METEORS);
    memset(&gc->projectiles, 0, sizeof(Projectile) * MAX_PROJECTILES);
    memset(&gc->pscluster, 0, sizeof(PSystem) * MAX_PARTICLE_CLUSTER);

    al_set_timer_speed(gc->timers.shtimer, 1);
    al_set_timer_count(gc->timers.shtimer, 0);
    al_start_timer(gc->timers.shtimer);
    al_set_timer_count(gc->timers.game_timer, 0);
    al_start_timer(gc->timers.game_timer);
}

void difficulty_update(GameComponents* gc) {
    if (gc->mt_base_speed < 500) {
        ++gc->mt_base_speed;
    }
    if (gc->timers.shfreq > 0.1) {
        gc->timers.shfreq -= 0.0020;
        al_set_timer_speed(gc->timers.shtimer, gc->timers.shfreq);
    }
    if (gc->player.ebar.depl_rate > 0.005) {
        gc->player.ebar.depl_rate += 0.00015;
    }
}
