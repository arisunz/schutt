/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "floatrect.h"

bool rect_intersect(const FloatRect* rect1, const FloatRect* rect2) {
    if (rect1->left + rect1->width < rect2->left ||
        rect2->left + rect2->width < rect1->left ||
        rect1->top + rect1->height < rect2->top  ||
        rect2->top + rect2->height < rect1->top) {
        return false;
    } else {
        return true;
    }
}

bool point_intersect(const FloatRect* this_rect, const Vector2f* point) {
    if (point->x >= this_rect->left &&
        point->x <= this_rect->left + this_rect->width &&
        point->y >= this_rect->top &&
        point->y <= this_rect->top + this_rect->height) {
        return true;
    } else {
        return false;
    }
}
