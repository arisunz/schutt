/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include <allegro5/allegro.h>
#include "floatrect.h"

#define METEOR_SIZE_MAX 25
#define METEOR_SIZE_MIN 15

typedef struct {
    ALLEGRO_COLOR color;
    Vector2f position;
    uint32_t speed;
    float radius;
    FloatRect hitbox;
    bool active;
    bool shooting_up;
} Meteor;

void meteor_shoot(Meteor* mt, uint32_t speed, bool shooting_up, uint32_t seed);

void meteor_move(Meteor* mt, float yoffset);
