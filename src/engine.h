/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>

#include "globdefs.h"
#include "player.h"
#include "projectile.h"
#include "meteor.h"
#include "particle.h"
#include "palette.h"

#define MAX_PROJECTILES 20
#define MAX_METEORS 50
#define MAX_PARTICLE_CLUSTER 10

#define KEY_SEEN 1
#define KEY_RELEASED 2

typedef struct {
    ALLEGRO_TIMER* timer;
    ALLEGRO_EVENT_QUEUE* queue;
    ALLEGRO_DISPLAY* display;
    ALLEGRO_BITMAP* window_icon;
} BaseComponents;

typedef enum {
    GAME_START,
    RUNNING,
    PAUSED,
    GAME_OVER
} GameState;

typedef struct {
    ALLEGRO_FONT* font15pt;
    ALLEGRO_FONT* font20pt;
    ALLEGRO_FONT* font40pt;
    ALLEGRO_FONT* font40pt_bold;
} GameUI;

typedef struct {
    ALLEGRO_TIMER* shtimer;
    ALLEGRO_TIMER* game_timer;
    uint32_t session_time;
    double shfreq;
} GTimers;

typedef struct {
    ALLEGRO_SAMPLE* shoot;
    ALLEGRO_SAMPLE* expl;
    ALLEGRO_SAMPLE* toggle;
    ALLEGRO_SAMPLE* bgm;
    ALLEGRO_SAMPLE_INSTANCE* bgm_instance;
} SoundBank;

typedef struct {
    uint8_t key[ALLEGRO_KEY_MAX];
    uint64_t score;
    uint32_t mt_base_speed;
    GameState state;
    Player player;
    GTimers timers;
    ALLEGRO_BITMAP* wall;
    ALLEGRO_BITMAP* scale_buffer;
    Vector2f scale_res;
    Projectile projectiles[MAX_PROJECTILES];
    Meteor meteors[MAX_METEORS];
    PSystem pscluster[MAX_PARTICLE_CLUSTER];
    GameUI gameui;
    BiPalette palette;
    SoundBank sounds;
} GameComponents;

bool base_components_init(BaseComponents* cmps);
int32_t engine_run();
void engine_finalize(BaseComponents* cmps);

bool game_components_init(GameComponents* gc);
void game_cleanup(GameComponents* gc);

void frame_input(GameComponents* gc, bool using_wasd);
void key_input(GameComponents* gc, const ALLEGRO_EVENT* ev, bool using_wasd);
bool gameover_input(GameComponents* gc);
bool pause_input(GameComponents* gc);
bool start_input(GameComponents* gc);

void random_meteor_shoot(Meteor mt[static MAX_METEORS], uint32_t speed, uint32_t seed);
void gc_update(GameComponents* gc);
void collision_check(GameComponents* gc);
void difficulty_update(GameComponents* gc);

void gc_draw(GameComponents* gc, ALLEGRO_DISPLAY* display);
void menu_draw(GameComponents* gc, bool using_wasd);

void game_reset(GameComponents* gc, ALLEGRO_DISPLAY* display);

bool is_initial_resolution(GameComponents* gc);
void resolution_swap(GameComponents* gc);

void toggle_music(GameComponents* gc);
