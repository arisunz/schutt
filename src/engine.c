/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include "engine.h"
#include "floatrect.h"


bool base_components_init(BaseComponents* cmps) {
    al_init();
    al_install_keyboard();
    al_install_audio();
    al_init_acodec_addon();
    al_init_font_addon();
    al_init_ttf_addon();
    al_init_primitives_addon();
    al_init_image_addon();

    cmps->timer = al_create_timer(1.0 / MAX_FPS);
    cmps->queue = al_create_event_queue();
    cmps->display = al_create_display(SCREEN_W, SCREEN_H);

    al_set_window_title(cmps->display, "SCHÜTT");

    cmps->window_icon = al_load_bitmap("assets/gfx/icon.png");
    al_set_display_icon(cmps->display, cmps->window_icon);

    if (cmps->timer && cmps->queue && cmps->display && cmps->window_icon) {
        return true;
    } else {
        return false;
    }
}

bool game_components_init(GameComponents* gc) {
    al_set_new_bitmap_flags(ALLEGRO_MIN_LINEAR);
    palette_get(&gc->palette);
    player_init(&gc->palette, &gc->player);

    if (!al_reserve_samples(4)) {
        return false;
    }

    gc->sounds.shoot = al_load_sample("assets/sounds/pew.ogg");
    gc->sounds.expl = al_load_sample("assets/sounds/expl.ogg");
    gc->sounds.toggle = al_load_sample("assets/sounds/toggle.ogg");
    gc->sounds.bgm = al_load_sample("assets/sounds/bgm.ogg");

    if (!gc->sounds.shoot || !gc->sounds.expl || !gc->sounds.bgm) {
        return false;
    }

    gc->sounds.bgm_instance = al_create_sample_instance(gc->sounds.bgm);
    al_set_sample_instance_playmode(gc->sounds.bgm_instance, ALLEGRO_PLAYMODE_LOOP);
    al_attach_sample_instance_to_mixer(gc->sounds.bgm_instance, al_get_default_mixer());


    gc->mt_base_speed = 200;

    gc->gameui.font15pt = al_load_ttf_font("assets/fonts/FiraMono-Medium.otf", 15, 0);
    gc->gameui.font20pt = al_load_ttf_font("assets/fonts/FiraMono-Medium.otf", 20, 0);
    gc->gameui.font40pt = al_load_ttf_font("assets/fonts/FiraMono-Medium.otf", 40, 0);
    gc->gameui.font40pt_bold = al_load_ttf_font("assets/fonts/FiraMono-Bold.otf", 40, 0);

    if (!gc->gameui.font15pt || !gc->gameui.font20pt ||
        !gc->gameui.font40pt || !gc->gameui.font40pt_bold) {
        return false;
    }

    gc->timers.shfreq = 1;
    gc->timers.shtimer = al_create_timer(gc->timers.shfreq);

    gc->timers.session_time = 0;
    gc->timers.game_timer = al_create_timer(1);

    gc->wall = al_create_bitmap(SCREEN_W, SCREEN_H);
    al_set_target_bitmap(gc->wall);
    al_clear_to_color(gc->palette.bg);

    gc->scale_buffer = al_create_bitmap(SCREEN_W, SCREEN_H);
    al_set_target_bitmap(gc->scale_buffer);
    al_clear_to_color(gc->palette.bg);

    gc->scale_res.x = SCREEN_W;
    gc->scale_res.y = SCREEN_H;

    return true;
}

int32_t engine_run() {
    BaseComponents cmps = {NULL};
    GameComponents gc = {0};
    if (!base_components_init(&cmps) || !game_components_init(&gc)) {
        fprintf(stderr, "Failed to initialize one or more vital components.\n");
        game_cleanup(&gc);
        engine_finalize(&cmps);
        return EXIT_FAILURE;
    }

    al_set_target_backbuffer(cmps.display);
    gc.state = GAME_START;

    al_register_event_source(cmps.queue, al_get_keyboard_event_source());
    al_register_event_source(cmps.queue, al_get_display_event_source(cmps.display));
    al_register_event_source(cmps.queue, al_get_timer_event_source(cmps.timer));
    al_register_event_source(cmps.queue, al_get_timer_event_source(gc.timers.shtimer));

    bool done = false;
    bool update = false;
    bool draw = false;
    bool using_wasd = false;
    ALLEGRO_EVENT event;

    al_start_timer(cmps.timer);
    al_start_timer(gc.timers.shtimer);
    al_start_timer(gc.timers.game_timer);
    while (!done) {
        al_wait_for_event(cmps.queue, &event);

        switch (event.type) {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
                done = true;
                break;
            case ALLEGRO_EVENT_TIMER:
                if (event.timer.source == cmps.timer) {
                    if (gc.key[ALLEGRO_KEY_ESCAPE]) {
                        done = true;
                        break;
                    }

                    switch (gc.state) {
                        case RUNNING:
                            if (!gc.player.expired) {
                                frame_input(&gc, using_wasd);
                            }
                            update = true;
                            draw = true;
                            break;
                        case GAME_START:
                        case PAUSED:
                        case GAME_OVER:
                            draw = true;
                            update = true;
                            break;
                    }

                    for (uint32_t i = 0; i < ALLEGRO_KEY_MAX; ++i) {
                        gc.key[i] &= KEY_SEEN;
                    }
                    break;
                } else { // tick/second timer event
                    if (gc.state == RUNNING) {
                        random_meteor_shoot(gc.meteors, gc.mt_base_speed, al_get_timer_count(cmps.timer));
                    }
                }
                break;

            case ALLEGRO_EVENT_KEY_DOWN:
                gc.key[event.keyboard.keycode] = KEY_SEEN | KEY_RELEASED;
                // switch resolution at any time
                if (gc.key[ALLEGRO_KEY_F4]) {
                    resolution_swap(&gc);
                }

                if (gc.key[ALLEGRO_KEY_F3]) {
                    using_wasd = !using_wasd;
                    al_play_sample(gc.sounds.toggle, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                }

                switch (gc.state) {
                    case RUNNING:
                        if (!gc.player.expired) {
                            key_input(&gc, &event, using_wasd);
                        }
                        break;
                    case GAME_OVER:
                        if (gameover_input(&gc)) {
                            game_reset(&gc, cmps.display);
                            if (al_get_sample_instance_playing(gc.sounds.bgm_instance)) {
                                al_set_sample_instance_gain(gc.sounds.bgm_instance, 0.8);
                            }
                        }
                        break;
                    case PAUSED:
                        if (pause_input(&gc)) {
                            gc.state = RUNNING;
                            if (al_get_sample_instance_playing(gc.sounds.bgm_instance)) {
                                al_set_sample_instance_gain(gc.sounds.bgm_instance, 0.8);
                            }
                        }
                        break;
                    case GAME_START:
                        if (start_input(&gc)) {
                            gc.state = RUNNING;
                            al_set_sample_instance_gain(gc.sounds.bgm_instance, 0.8);
                            al_play_sample_instance(gc.sounds.bgm_instance);
                        }
                        break;
                }
                break;
            case ALLEGRO_EVENT_KEY_UP:
                gc.key[event.keyboard.keycode] &= KEY_RELEASED;
                break;
        }

        if (update && al_event_queue_is_empty(cmps.queue)) {
            if (gc.scale_res.x != al_get_display_width(cmps.display)) {
                al_resize_display(cmps.display, gc.scale_res.x, gc.scale_res.y);
            }
            switch (gc.state) {
                case RUNNING:
                    al_resume_timer(gc.timers.game_timer);
                    al_resume_timer(gc.timers.shtimer);

                    gc_update(&gc);
                    collision_check(&gc);

                    if (!gc.player.expired) {
                        gc.timers.session_time = al_get_timer_count(gc.timers.game_timer);
                    }

                    if (gc.timers.session_time > 0 && gc.timers.session_time % 10 == 0) {
                        difficulty_update(&gc);
                    }
                    update = false;
                    break;
                case GAME_START:
                case PAUSED:
                case GAME_OVER:
                    al_stop_timer(gc.timers.game_timer);
                    al_stop_timer(gc.timers.shtimer);
                    update = false;
                    break;
            }
        }

        if (draw && al_event_queue_is_empty(cmps.queue)) {
            if (!is_initial_resolution(&gc)) {
                al_set_target_bitmap(gc.scale_buffer);
            }

            al_clear_to_color(gc.palette.bg);

            switch (gc.state) {
                case RUNNING:
                case GAME_OVER:
                case PAUSED:
                    gc_draw(&gc, cmps.display);
                    break;
                case GAME_START:
                    menu_draw(&gc, using_wasd);
                    break;
            }

            if (!is_initial_resolution(&gc)) {
                al_set_target_backbuffer(cmps.display);
                al_draw_scaled_bitmap(
                    gc.scale_buffer,
                    0, 0, SCREEN_W, SCREEN_H,
                    0, 0, SCREEN_W * 1.25, SCREEN_H * 1.25, 0
                );
            }

            al_flip_display();
            draw = false;
        }
    }

    game_cleanup(&gc);
    engine_finalize(&cmps);
    return EXIT_SUCCESS;
}

void engine_finalize(BaseComponents* cmps) {
    if (cmps->timer) {
        al_destroy_timer(cmps->timer);
    }

    if (cmps->queue) {
        al_destroy_event_queue(cmps->queue);
    }

    if (cmps->display) {
        al_destroy_display(cmps->display);
    }

    if (cmps->window_icon) {
        al_destroy_bitmap(cmps->window_icon);
    }
}

void game_cleanup(GameComponents* gc) {
    if (gc->sounds.bgm) {
        al_destroy_sample(gc->sounds.bgm);
    }

    if (gc->sounds.expl) {
        al_destroy_sample(gc->sounds.expl);
    }

    if (gc->sounds.shoot) {
        al_destroy_sample(gc->sounds.shoot);
    }

    if (gc->sounds.toggle) {
        al_destroy_sample(gc->sounds.toggle);
    }

    if (gc->sounds.bgm_instance) {
        al_destroy_sample_instance(gc->sounds.bgm_instance);
    }

    if (gc->gameui.font15pt) {
        al_destroy_font(gc->gameui.font15pt);
    }

    if (gc->gameui.font20pt) {
        al_destroy_font(gc->gameui.font20pt);
    }

    if (gc->gameui.font40pt) {
        al_destroy_font(gc->gameui.font40pt);
    }

    if (gc->scale_buffer) {
        al_destroy_bitmap(gc->scale_buffer);
    }

    if (gc->wall) {
        al_destroy_bitmap(gc->wall);
    }

    if (gc->timers.game_timer) {
        al_destroy_timer(gc->timers.game_timer);
    }

    if (gc->timers.shtimer) {
        al_destroy_timer(gc->timers.shtimer);
    }
}

inline bool is_initial_resolution(GameComponents* gc) {
    return gc->scale_res.x == SCREEN_W;
}

void resolution_swap(GameComponents* gc) {
    if (is_initial_resolution(gc)) {
        gc->scale_res.x = SCREEN_W * 1.25;
        gc->scale_res.y = SCREEN_H * 1.25;
    } else {
        gc->scale_res.x = SCREEN_W;
        gc->scale_res.y = SCREEN_H;
    }
}

void toggle_music(GameComponents* gc) {
    bool toggle = al_get_sample_instance_playing(gc->sounds.bgm_instance);

    al_set_sample_instance_playing(gc->sounds.bgm_instance, !toggle);
}
