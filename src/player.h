/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <allegro5/allegro5.h>
#include <allegro5/allegro_primitives.h>

#include "particle.h"
#include "floatrect.h"
#include "palette.h"
#include "ebar.h"

#define PLAYER_ACCEL 500

typedef struct {
    ALLEGRO_VERTEX shape[4];
    Vector2f outline[4];
    Vector2f position;
    FloatRect hitbox;
    EBar ebar;
    bool expired;
    PSystem* expired_explosion;
} Player;

void player_init(BiPalette* palette, Player* pl);

void player_move(Player* pl, float xoffset);

Vector2f player_get_center(Player* pl);

void player_expire(Player* pl, PSystem* expl);
