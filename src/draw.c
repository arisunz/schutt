/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <inttypes.h>
#include "engine.h"

void gc_draw(GameComponents* gc, ALLEGRO_DISPLAY* display) {
    al_draw_bitmap(gc->wall, 0, 0, 0);
    /*
     * particles
     */
    for (uint32_t i = 0; i < MAX_PARTICLE_CLUSTER; ++i) {
        if (gc->pscluster[i].active) {
            al_draw_prim(
                &gc->pscluster[i],
                NULL, NULL,
                0, MAX_PARTICLES * 3,
                ALLEGRO_PRIM_TRIANGLE_LIST
            );
        } else if (!gc->pscluster[i].active && lifetime_over(&gc->pscluster[i])) {
            al_set_target_bitmap(gc->wall);
            al_draw_prim(
                &gc->pscluster[i],
                NULL, NULL,
                0, MAX_PARTICLES * 3,
                ALLEGRO_PRIM_TRIANGLE_LIST
            );
            gc->pscluster[i].lifetime = 0;
            if (!is_initial_resolution(gc)) {
                al_set_target_bitmap(gc->scale_buffer);
            } else {
                al_set_target_backbuffer(display);
            }
        }
    }

    /*
     * projectiles
     */
    for (uint32_t i = 0; i < MAX_PROJECTILES; ++i) {
        if (gc->projectiles[i].active) {
            al_draw_line(
                gc->projectiles[i].proj_start.x,
                gc->projectiles[i].proj_start.y,
                gc->projectiles[i].proj_tail.x,
                gc->projectiles[i].proj_tail.y,
                gc->palette.fg, 3.5
            );
        }
    }

    /*
     * meteors
     */
    for (uint32_t i = 0; i < MAX_METEORS; ++i) {
        if (gc->meteors[i].active) {
            al_draw_filled_circle(
                gc->meteors[i].position.x,
                gc->meteors[i].position.y,
                gc->meteors[i].radius,
                gc->palette.fg
            );
        }
    }

    /*
     * player
     */
    if (!gc->player.expired) {
        al_draw_prim(&gc->player.shape, NULL, NULL, 0, 4, ALLEGRO_PRIM_TRIANGLE_STRIP);

        al_draw_polygon(
            (float*) gc->player.outline,
            4,
            ALLEGRO_LINE_JOIN_MITER,
            gc->palette.fg,
            3,
            4.0
        );
    }

    switch (gc->state) {
        case RUNNING:
            /*
             * ebar
             */
            al_draw_filled_rectangle(
                gc->player.ebar.bar.left,
                gc->player.ebar.bar.top,
                gc->player.ebar.bar.left + gc->player.ebar.bar.width,
                gc->player.ebar.bar.top + gc->player.ebar.bar.height,
                gc->palette.fg_transp
            );

            /*
             * score
             */
            al_draw_filled_rectangle(5, 10, 185, 30, gc->palette.bg_transp);
            al_draw_textf(
                gc->gameui.font15pt,
                gc->palette.fg,
                10, 10, 0,
                "%019" PRIu64, gc->score
            );

            /*
             * time
             */
            al_draw_filled_rectangle(5, 30, 60, 50, gc->palette.bg_transp);
            al_draw_textf(
                gc->gameui.font15pt,
                gc->palette.fg,
                10, 30, 0,
                "%02" PRIu32 ":%02" PRIu32,
                gc->timers.session_time / 60,
                gc->timers.session_time > 59 ? gc->timers.session_time % 60 : gc->timers.session_time
            );
            break;
        case GAME_OVER:
            al_draw_filled_rectangle(
                0, 0, SCREEN_W, SCREEN_H,
                gc->palette.bg_transp
            );
            al_draw_text(
                gc->gameui.font40pt,
                gc->palette.fg,
                30, 30, 0,
                "Shoot!"
            );
            al_draw_text(
                gc->gameui.font20pt,
                gc->palette.fg,
                60, 100, 0,
                "You woefully survived for"
            );
            if (gc->timers.session_time / 60 == 0) {
                al_draw_textf(
                    gc->gameui.font20pt,
                    gc->palette.fg,
                    60, 130, 0,
                    "%" PRIu32 " seconds,",
                    gc->timers.session_time > 59 ? gc->timers.session_time % 60 : gc->timers.session_time
                );
            } else if (gc->timers.session_time / 60 == 1) {
                al_draw_textf(
                    gc->gameui.font20pt,
                    gc->palette.fg,
                    60, 130, 0,
                    "%" PRIu32 " minute and %" PRIu32 " seconds,",
                    gc->timers.session_time / 60,
                    gc->timers.session_time > 59 ? gc->timers.session_time % 60 : gc->timers.session_time
                );
            } else {
                al_draw_textf(
                    gc->gameui.font20pt,
                    gc->palette.fg,
                    60, 130, 0,
                    "%" PRIu32 " minutes and %" PRIu32 " seconds,",
                    gc->timers.session_time / 60,
                    gc->timers.session_time > 59 ? gc->timers.session_time % 60 : gc->timers.session_time
                );
            }
            al_draw_textf(
                gc->gameui.font20pt,
                gc->palette.fg,
                60, 160, 0,
                "with a score of %" PRIu64,
                gc->score
            );
            al_draw_text(
                gc->gameui.font20pt,
                gc->palette.fg,
                30, 400, 0,
                "Press ENTER to try again, or ESC to quit."
            );
            break;
        case PAUSED:
            al_draw_filled_rectangle(
                0, 0, SCREEN_W, SCREEN_H,
                gc->palette.bg_transp
            );
            al_draw_text(
                gc->gameui.font20pt,
                gc->palette.fg,
                30, SCREEN_H / 2, 0,
                "Paused..."
            );
            break;
        case GAME_START:
            break;
    }
}

void menu_draw(GameComponents* gc, bool using_wasd) {
    al_draw_text(
        gc->gameui.font40pt_bold,
        gc->palette.fg,
        30, 100, 0,
        "S C H Ü T T"
    );

    al_draw_text(
        gc->gameui.font15pt,
        gc->palette.fg,
        30, 160, 0,
        "A minimal game by arisunz"
    );

    if (!using_wasd) {
        al_draw_text(
            gc->gameui.font20pt,
            gc->palette.fg,
            30, 380, 0,
            "F3: ↑ ← ↓ →"
        );
    } else {
        al_draw_text(
            gc->gameui.font20pt,
            gc->palette.fg,
            30, 380, 0,
            "F3: W A S D"
        );
    }

    al_draw_text(
        gc->gameui.font20pt,
        gc->palette.fg,
        30, 410, 0,
        "SHIFT to slow down"
    );

    al_draw_text(
        gc->gameui.font20pt,
        gc->palette.fg,
        30, 440, 0,
        "ENTER to begin || ESC to quit"
    );

    al_draw_text(
        gc->gameui.font15pt,
        gc->palette.fg,
        SCREEN_W - 90, 10, 0,
        "v0.9.5"
    );
}
