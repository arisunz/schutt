/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "ebar.h"
#include "globdefs.h"

void ebar_init(EBar* ebar) {
    ebar->bar.top = SCREEN_H - 30;
    ebar->bar.left = 10;
    ebar->bar.height = 10;
    ebar->bar.width = SCREEN_W - 20;

    ebar->depl_rate = 0.001;
}

void ebar_refill(EBar* ebar, float refill) {
    ebar->bar.width += refill;
    if (ebar->bar.width > SCREEN_W - 20) {
        ebar->bar.width = SCREEN_W - 20;
    }
}

bool ebar_update(EBar* ebar) {
    float offset = ebar->bar.width - (MAX_EBAR_WIDTH * ebar->depl_rate);
    if (offset > 0) {
        ebar->bar.width = offset;
        return false;
    } else {
        // return true if the bar has depleted,
        // causing a GAME_OVER
        return true;
    }
}
