/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "projectile.h"
#include "globdefs.h"

void projectile_shoot(Projectile* proj, Vector2f init_pos, bool shooting_up) {
    proj->proj_start.x = init_pos.x;
    proj->proj_start.y = init_pos.y;

    proj->proj_tail.x = init_pos.x;
    proj->proj_tail.y = shooting_up ? init_pos.y + 10 : init_pos.y - 10;

    proj->active = true;
    proj->shooting_up = shooting_up;
}

void projectile_move(Projectile* proj, float offset) {
    if (proj->active) {
        if (proj->shooting_up) {
            proj->proj_start.y += -offset;
            proj->proj_tail.y += -offset;
        } else {
            proj->proj_start.y += offset;
            proj->proj_tail.y += offset;
        }

        if (proj->proj_start.y < - 10 || proj->proj_start.y > SCREEN_H - 10) {
            proj->active = false;
        }
    }
}
