/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "palette.h"

void palette_get(BiPalette* palette) {
    srand(time(NULL));
    bool light_on_dark = rand() % 2 == 0 ? true : false;

    float fghue = rand() % 361;
    float bghue = fghue + 180;
    if (bghue > 360) {
        bghue = bghue - 360;
    }

    float fgsat, bgsat, fgval, bgval;

    if (light_on_dark) {
        fgsat = 0.4;
        bgsat = 0.7;

        fgval = 0.6;
        bgval = 0.1;
    } else {
        fgsat = 0.7;
        bgsat = 0.2;

        fgval = 0.3;
        bgval = 0.8;
    }

    palette->fg = al_color_hsv(fghue, fgsat, fgval);
    palette->bg = al_color_hsv(bghue, bgsat, bgval);

    float r, g, b;
    if (light_on_dark) {
        al_color_hsv_to_rgb(bghue, bgsat, bgval - 0.5, &r, &g, &b);
        palette->bg_transp = al_map_rgba_f(r, g, b, 0.3);

        al_color_hsv_to_rgb(fghue, fgsat, fgval - 0.4, &r, &g, &b);
        palette->fg_transp = al_map_rgba_f(r, g, b, 0.3);
    } else {
        al_color_hsv_to_rgb(bghue, bgsat, bgval - 0.5, &r, &g, &b);
        palette->bg_transp = al_map_rgba_f(r, g, b, 0.3);

        al_color_hsv_to_rgb(fghue, fgsat, fgval, &r, &g, &b);
        palette->fg_transp = al_map_rgba_f(r, g, b, 0.5);
    }
}
