/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include "floatrect.h"

#define MAX_EBAR_WIDTH (SCREEN_W + 20)

typedef struct {
    FloatRect bar;
    float depl_rate; // depletion rate per frame
} EBar;

void ebar_init(EBar* ebar);

void ebar_refill(EBar* ebar, float refill);

bool ebar_deplete(EBar* ebar, float offset);

bool ebar_update(EBar* ebar);
