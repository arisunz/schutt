/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "engine.h"

void frame_input(GameComponents* gc, bool using_wasd) {
    float xoffset = PLAYER_ACCEL * (1.0 / MAX_FPS);
    if (!using_wasd) {
        if (gc->key[ALLEGRO_KEY_LEFT] || gc->key[ALLEGRO_KEY_RIGHT]) {
            player_move(
                &gc->player,
                (gc->key[ALLEGRO_KEY_RIGHT] ? xoffset : -xoffset) * (gc->key[ALLEGRO_KEY_LSHIFT] ? 0.5 : 1.0)
            );
        }
    } else {
        if (gc->key[ALLEGRO_KEY_A] || gc->key[ALLEGRO_KEY_D]) {
            player_move(
                &gc->player,
                (gc->key[ALLEGRO_KEY_D] ? xoffset : -xoffset) * (gc->key[ALLEGRO_KEY_RSHIFT] ? 0.5 : 1.0)
            );
        }
    }
}

void key_input(GameComponents* gc, const ALLEGRO_EVENT* ev, bool using_wasd) {
    if (ev->keyboard.keycode == ALLEGRO_KEY_ENTER) {
        gc->state = PAUSED;
        if (al_get_sample_instance_playing(gc->sounds.bgm_instance)) {
            al_set_sample_instance_gain(gc->sounds.bgm_instance, 0.4);
        }
    }

    if (ev->keyboard.keycode == ALLEGRO_KEY_F2) {
        toggle_music(gc);
    }

    if (!using_wasd) {
        if (ev->keyboard.keycode == ALLEGRO_KEY_UP || ev->keyboard.keycode == ALLEGRO_KEY_DOWN) {
            for (uint32_t i = 0; i < MAX_PROJECTILES; ++i) {
                if (!gc->projectiles[i].active) {
                    projectile_shoot(
                        &gc->projectiles[i],
                        gc->player.position,
                        ev->keyboard.keycode == ALLEGRO_KEY_UP ? true : false
                    );
                    al_play_sample(gc->sounds.shoot, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    break;
                }
            }
        }
    } else {
        if (ev->keyboard.keycode == ALLEGRO_KEY_W || ev->keyboard.keycode == ALLEGRO_KEY_S) {
            for (uint32_t i = 0; i < MAX_PROJECTILES; ++i) {
                if (!gc->projectiles[i].active) {
                    projectile_shoot(
                        &gc->projectiles[i],
                        gc->player.position,
                        ev->keyboard.keycode == ALLEGRO_KEY_W ? true : false
                    );
                    al_play_sample(gc->sounds.shoot, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                    break;
                }
            }
        }
    }
}

/*
 * Yes, these three functions do almost exactly
 * the same thing but let's keep them for clarity's sake
 */
bool gameover_input(GameComponents* gc) {
    if (gc->key[ALLEGRO_KEY_ENTER]) {
        return true;
    }
    else return false;
}

bool pause_input(GameComponents* gc) {
    if (gc->key[ALLEGRO_KEY_F2]) {
        toggle_music(gc);
    }

    if (gc->key[ALLEGRO_KEY_ENTER]) {
        return true;
    } else return false;
}

bool start_input(GameComponents* gc) {
    if (gc->key[ALLEGRO_KEY_ENTER]) {
        return true;
    }

    else return false;
}
