/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <stdbool.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include "vector2f.h"

#define MAX_PARTICLES 30
#define PARTICLE_MAX_LIFETIME 60

typedef struct {
    ALLEGRO_VERTEX shapes[MAX_PARTICLES * 3];
    Vector2f direction[MAX_PARTICLES];
    int64_t lifetime;
    bool active;
} PSystem;

void expl_create(PSystem* psystem, Vector2f pos, ALLEGRO_COLOR color);
void expl_update(PSystem* psystem);
bool lifetime_over(PSystem* psystem);
