/*
 *  This file is part of SCHÜTT
 *
 *  SCHÜTT - (C) 2018 Ariela Wenner - <arisunz@disroot.org>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "player.h"
#include "globdefs.h"

void player_init(BiPalette* palette, Player* pl) {
    pl->position.x = SCREEN_W / 2.0;
    pl->position.y = SCREEN_H / 2.0;

    pl->shape[0].x = pl->position.x;
    pl->shape[0].y = pl->position.y - 12;
    pl->shape[0].color = palette->bg;

    pl->shape[1].x = pl->position.x - 7;
    pl->shape[1].y = pl->position.y;
    pl->shape[1].color = palette->bg;

    pl->shape[2].x = pl->position.x + 7;
    pl->shape[2].y = pl->position.y;
    pl->shape[2].color = palette->bg;

    pl->shape[3].x = pl->position.x;
    pl->shape[3].y = pl->position.y + 12;
    pl->shape[3].color = palette->bg;

    pl->outline[0].x = pl->position.x;
    pl->outline[0].y = pl->position.y - 13;

    pl->outline[1].x = pl->position.x - 8;
    pl->outline[1].y = pl->position.y;

    pl->outline[2].x = pl->position.x;
    pl->outline[2].y = pl->position.y + 13;

    pl->outline[3].x = pl->position.x + 8;
    pl->outline[3].y = pl->position.y;

    pl->hitbox.left = pl->position.x - 1;
    pl->hitbox.top = pl->position.y - 1;
    pl->hitbox.width = 2;
    pl->hitbox.height = 2;

    pl->expired = false;
    pl->expired_explosion = NULL;

    ebar_init(&pl->ebar);
}

void player_move(Player* pl, float xoffset) {
    if (pl->position.x + xoffset > 640 || pl->position.x + xoffset < 0) {
        return;
    }

    pl->position.x += xoffset;

    pl->shape[0].x += xoffset;
    pl->shape[1].x += xoffset;
    pl->shape[2].x += xoffset;
    pl->shape[3].x += xoffset;

    pl->outline[0].x += xoffset;
    pl->outline[1].x += xoffset;
    pl->outline[2].x += xoffset;
    pl->outline[3].x += xoffset;

    pl->hitbox.left += xoffset;
}

Vector2f player_get_center(Player* pl) {
    return pl->position;
}
