# Schütt

Minimalist shoot 'em up game focused on precise shooting and dodging.


# Building

## Requirements
* Allegro 5 + color, primitive, font, TTF, audio and codec addons.
* A recent C compiler (I only tested on GCC).
* GNU Make.


## Unix-like systems
Check your dependencies, run `make CFLAGS="<preferred flags>"` and run the executable `schutt`.

## Windows
Unfortunately, I don't have a machine with Windows on it to test. Your best bet is probably loading the source into your IDE of preference, [downloading the required libraries](https://liballeg.org/download.html#windows) and configuring your project accordingly.
[Here](https://wiki.allegro.cc/index.php?title=Getting_Started#Windows) is a list of HowTo's that might be useful.

## (to) Windows
If you wish to cross-compile to Windows, get yourself the `mingw64` toolchain and fetch the required libraries+dependencies from [here](https://github.com/liballeg/allegro5/releases) and [here](https://github.com/liballeg/allegro_winpkg/releases).
The included makefile has options for building with `buildw32` and `buildw64`, and packaging with `packagew32` and `packagew64` for 32 and 64-bit systems, respectively.

Your directory layout should look like this:

    schutt
    └─ mingw
        ├── i686
        │   ├── bin
        │   ├── include
        │   └── lib
        └── x86_64
            ├── bin
            ├── include
            └── lib


# Licenses

© Ariela Wenner - 2018

Code is licenced under the GNU GPL version 3 or newer. See [COPYING](COPYING.md) for details.

Assets including music and sound effects are licenced under a [Creative Commons BY-NC-SA license](https://creativecommons.org/licenses/by-nc-sa/4.0/).


The included font (Fira Mono) is licensed under the [SIL Open Font License](https://scripts.sil.org/OFL).

* Fira is a trademark of The Mozilla Corporation.
* Digitized data copyright 2012-2018, The Mozilla Foundation and Telefonica S.A., bBox Type GmbH and Carrois Corporate GbR, with Reserved Font Name "Fira"
* Design 2012-2015: Carrois Corporate GbR & Edenspiekermann AG
* Design 2016 and later: bBox Type GmbH
